package nl.utwente.di.bookQuote;

public class Quoter {
    public double getBookPrice(String isbn) {
        switch (isbn){
            case "1":
                return 10.0d;
            case "2":
                return 45.0d;
            case "3":
                return 20.0d;
            case "4":
                return 35.0d;
            case "5":
                return 50.0d;
            default:
                return 0.0d;
        }
    }
}
